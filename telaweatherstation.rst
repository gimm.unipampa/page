
.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Weather Station
================
	
Construção, automatização e operação de equipamento para o monitoramento de variáveis meteorológicas e de partículas respiráveis e inaláveis suspensas na atmosfera.
A poluição atmosférica apesar de não ser considerada uma temática nova, deve ser constantemente investigada, uma vez que cerca de 7 milhões de pessoas morrem anualmente por suas consequências. Além disso, quase toda a população do mundo (99%) respira ar que excede os limites de qualidade recomendados pelas Diretrizes Globais de Qualidade do Ar, estabelecidas pela Organização Mundial de Saúde (OMS). Nesse contexto, apesar de possuir um aparato legislativo e também um considerável Sistema Nacional de Meio Ambiente (SISNAMA), o Brasil ainda apresenta deﬁciências no âmbito da poluição atmosférica, pois muitos municípios do país não realizam monitoramento, diﬁcultando os processos de controle. O estado do Rio Grande do Sul, por exemplo, possui 497 municípios e destes, apenas 5 (Canoas, Esteio, Gravataí, Guaíba e Triunfo) monitoram a poluição atmosférica. O presente Projeto visa contribuir com a rede de monitoramento do Rio Grande do Sul, construindo um equipamento mais acessível ﬁnanceiramente, porém com a eﬁciência dos equipamentos que já são consolidados no mercado. Bagé seria o primeiro município monitorado na Região da Campanha Gaúcha, com pretensão de expandir para os demais municípios futuramente.



Monitoramento Acessível em `https://weatherstationbage.web.app/ <https://weatherstationbage.web.app/#>`_

Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
