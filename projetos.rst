.. image:: img/bannergimm.png

Projetos em Informática Médica
********************************

`ARFisio <telaarfisio.html#>`_
===================

o objetivo deste projeto é a construção de uma solução computacional utilizando realidade aumentada e um nó sensor, o qual possibilite a simulação de um conjunto de atividades necessárias ao processo de reabilitação física de pacientes com amputações em membros superiores. Desta forma, através da interatividade e imersão do paciente, estimulem áreas motoras do córtex e de feedback visual.. Vislumbra-se também, que este estudo disponibilize um instrumento válido para fisioterapeutas acompanharem a evolução de seus pacientes para que os mesmos tenham uma melhor adaptação a sociedade.
`Detalhar <telaarfisio.html#>`_


`Victus <telavictus.html#>`_
===================

O projeto Victus tem como propósito disponibilizar, aos profissionais da área de fisioterapia,uma ferramenta que possibilite o acompanhamento do progresso de pacientes ao longo do tratamento de reabilitação física. A proposta é o desenvolvimento de uma solução de fácil uso, adaptável para diferentes ambientes, diferentes tipos de pacientes e, também, de baixo custo. Desta forma, o sistema Victus foi projetado para análisar sessões de fisioterapia de indivíduos amputados de membros inferiores em uma bicicleta ergométrica.
`Detalhar <telavictus.html#>`_

`Victus-VR <telavictusvr.html#>`_
===================

O presente estudo aborda o processo de desenvolvimento e aplicação de uma ferramenta tecnológica para auxiliar o tratamento de indivíduos amputados do membro inferior. Essa ferramenta consiste em uma plataforma em software e hardware que compõem um jogo sério controlado por sensores distribuídos em uma bicicleta ergométrica. O jogo sério também é capaz de monitorar informações clínicas, tem o caráter motivacional e foi desenvolvido com base em técnicas de incentivo conceituadas nas áreas de jogos e psicologia. Com base na Teoria da Autodeterminação, Teoria do Flow, Gamificação e um
sistema de Ajuste Dinâmico de Dificuldade o Victus VR, como foi batizada a ferramenta, viabiliza uma maior motivação e engajamento durante o processo de reabilitação física de amputados. Esta pesquisa foi baseada em um trabalho previamente desenvolvido, a ferramenta Victus, que consiste em uma plataforma de monitoramento e tratamento de dados clínicos, também em pacientes amputados do membro inferior, que tem como principal objetivo a análise clínica mais precisa do progresso dos indivíduos no processo de reabilitação. Por tanto, as ferramentas Victus e Victus VR podem ser classificadas como complementares. Estas pesquisas foram desenvolvidas pelo Grupo de Informática Médica da Universidade Federal do Pampa e conta com o apoio de profissionais de fisioterapia do Serviço de Reabilitação Física de Bagé.
`Detalhar <telavictusvr.html#>`_


`Physiopong <telaphysiopong.html#>`_
===================

A aplicação intitulada PhysioPong, propõe uma solução computacional baseada no contexto de Serious Games (Jogos Sérios), para apoiar no processo de reabilitação física de pacientes amputados de membro superior. Dessa forma, espera-se fornecer uma ferramenta de treinamento, utilizando a plataforma de prototipação Arduino integrada a técnicas de monitoramento de sinais eletromiográficos para auxiliar nas primeiras fases do processo de reabilitação de pacientes. Assim, a solução computacional deverá possibilitar a simulação de um conjunto de atividades necessárias ao processo de reabilitação física  destes indivíduos. Vislumbra-se também, proporcionar ao paciente um ambiente motivador para o tratamento, bem como uma ferramenta válida para os fisioterapeutas acompanharem a evolução destes indivíduos.
`Detalhar <telaphysiopong.html#>`_


`FootInspect - Baropodometro <telabaropodometro.html#>`_
===================

O projeto FootInspect, tem por finalidade, a criação de um sistema computacional que dê suporte à fisioterapeutas e profissionais da área da saúde, ajudando na análise, diagnóstico, de pessoas que possuem problemas relacionados às regiões plantares (pés), visando a reabilitação física e a melhora da qualidade de vida dos indivíduos. Basicamente, a solução dispõe de uma palmilha sensível a pressão, de baixo custo, para assessorar sessões de fisioterapia através do monitoramento  da distribuição de pressão na planta do pé.
`Detalhar <telabaropodometro.html#>`_


`MFID <telamfid.html#>`_
===================

Sabe-se que atualmente a computação está cada vez mais integrada à outras áreas de conhecimento fornecendo soluções práticas e inovadoras para todos os tipos de problemas. Na área da saúde isso também ocorre, desde equipamentos para cirurgias, ferramentas para diagnósticos de doenças, até sistemas para monitoramento e processamento de dados. Nesse sentido, o processo de reabilitação física de pacientes amputados também se mostra como uma área importante para o desenvolvimento de ferramentas que auxiliem o profissional fisioterapeuta, visto que este processo de reabilitação geralmente é duradouro e exaustivo para os pacientes. Assim, o presente trabalho descreve o desenvolvimento de uma solução para o aperfeiçoamento do processo de reabilitação física de pacientes amputados, a partir da detecção da fadiga muscular durante as sessões de exercícios físicos, que uma vez não identificada, pode causar lesões e dores aos pacientes, prolongando ainda mais o tratamento. O sistema desenvolvido consiste na integração de sensores, da plataforma de prototipagem eletrônica arduino e de um software para a visualização e armazenamento dos dados coletados. Desse modo, foi possível realizar o monitoramento e a identificação da ocorrência da fadiga muscular durante as sessões de exercícios físicos realizados pelos pacientes, além de possibilitar ao fisioterapeuta uma ferramenta para avaliação do tratamento.
`Detalhar <telamfid.html#>`_


`Tennis Fisio <telatennisfisio.html#>`_
===================

O presente estudo descreve a proposta de baixo custo de uma solução computacional baseada em gamificação utilizando hardware e software, que seja portável para plataformas single boards, e possa auxiliar os profissionais de fisioterapia nas sessões de reabilitação de amputações dos membros superiores e inferiores. A finalidade deste trabalho é tratar o problema descrito utilizando como sensores os do tipo acelerômetro e giroscópio de um smartphone. Foram implementados tanto o app em Flutter para coordenar a comunicação com o nó sensor, quanto o nó sensor com a ajuda da plataforma de prototipação Arduino e módulos de comunicação. Por fim, também foi desenvolvido um jogo em Unity 3D para a simulação de uma partida de tênis de mesa, e um sistema de acompanhamento para os fisioterapeutas. As informações transmitidas a partir dos sensores do smartphone, servirão para a realização da movimentação da raquete virtual no jogo. Diferentes experimentos foram executados com o objetivo de validar a proposta. Os resultados demonstraram limitações na implementação que necessitaram sua refatoração. Logo após a refatoração os resultados iniciais demonstraram grande potencialidade na solução. O projeto visa criar uma solução que torne mais lúdica e motivadora as sessões de fisioterapia, tentando levar o paciente ao estado de flow. Ademais, os fisioterapeutas terão acesso a uma ferramenta de controle de desempenho de cada uma das sessões de cada paciente, o que poderá tornar menos subjetiva a análise de desempenho dos mesmos.
`Detalhar <telatennisfisio.html#>`_

`Physio Games <telaphysiogames.html#>`_
===================

O presente estudo descreve a proposta de baixo custo de uma solução computacional baseada em gamificação utilizando hardware e software, que seja portável para plataformas single boards, e possa auxiliar os profissionais de fisioterapia nas sessões de reabilitação de amputações dos membros superiores e inferiores. A finalidade deste trabalho é tratar o problema descrito utilizando como sensores os do tipo acelerômetro e giroscópio de um smartphone. Foram implementados tanto o app em Flutter para coordenar a comunicação com o nó sensor, quanto o nó sensor com a ajuda da plataforma de prototipação Arduino e módulos de comunicação. Por fim, também foi desenvolvido um jogo em Unity 3D para a simulação de uma partida de tênis de mesa, e um sistema de acompanhamento para os fisioterapeutas. As informações transmitidas a partir dos sensores do smartphone, servirão para a realização da movimentação da raquete virtual no jogo. Diferentes experimentos foram executados com o objetivo de validar a proposta. Os resultados demonstraram limitações na implementação que necessitaram sua refatoração. Logo após a refatoração os resultados iniciais demonstraram grande potencialidade na solução. O projeto visa criar uma solução que torne mais lúdica e motivadora as sessões de fisioterapia, tentando levar o paciente ao estado de flow. Ademais, os fisioterapeutas terão acesso a uma ferramenta de controle de desempenho de cada uma das sessões de cada paciente, o que poderá tornar menos subjetiva a análise de desempenho dos mesmos.
`Detalhar <telaphysiogames.html#>`_


`FisioLung <telafisiolung.html#>`_
===================
A fisioterapia respiratória é necessária para o tratamento de pacientes acometidos por doenças pulmonares. Nas sessões são aplicadas técnicas para a remoção de secreções das vias aéreas. A técnica de vibração torácica manual é extremamente importante para a higienização brônquica, em razão da dificuldade de execução da técnica, construiu-se uma ferramenta para análise de sua execução. Com um sistema de monitoramento a partir do registro de sessões, torna-se possível o acompanhamento da evolução do paciente e da aplicação da técnica de vibração. O sistema FisioLung é capaz de registrar as sessões de fisioterapia com o cadastro de dados pessoais do paciente e do profissional, são armazenadas as informações de anamnese, técnicas utilizadas e análise da técnica de vibração torácica manual. O FisioLung é baseado na integração de hardware e software, é utilizado um acelerômetro para captar os dados de vibração e uma placa Arduino para processá-los, o software da aplicação utiliza a arquitetura Model-View-Controller para integração entre o front-end e back-end. As análises de sessões tornam-se importantes para acompanhar a evolução do paciente ao decorrer do tratamento. Na tela de análise são exibidos os gráficos de picos de frequência versus tempo e da média dos picos de frequência versus tempo, além de informações das sessões como: tempo de análise, dados de anamnese e técnicas utilizadas. A modelagem foi realizada a partir dos requisitos definidos, prototipação de telas, casos de uso, digramas de classe e sequência, e banco de dados. De acordo com os testes funcionais realizados no sistema foi possível mensurar o desempenho, executando o esperado para a solução. O desenvolvimento do FisioLung permitiu uma melhora de performance da execução da técnica de vibração torácica, beneficiando os pacientes durante o tratamento. As análises das sessões permitiram um melhor acompanhamento do paciente pelos fisioterapeutas, identificando possíveis melhorias e recaídas.
`Detalhar <telafisiolung.html#>`_


`Ausculsensor <asculsensor.html#>`_
===================
o presente trabalho tem como objetivo desenvolver um sistema hábil para realização da etapa de ausculta pulmonar de forma automatizada, por meio da integração de hardware e software, sendo um nó sensor acoplado a um este-
toscópio, estruturado para capturar a frequência durante a etapa de ausculta pulmonar. Foi realizado uma exploração no espaço de projeto para a implementação do nó sensor considerando diferentes critérios, dentre eles, cita-se baixo custo versus qualidade do sinal obtido para a realização da ausculta pulmonar. Outro viés da ferramenta, é o auxilio no treinamento de estudantes da área da saúde, além da análise da ausculta através de técnicas de machine learning. Esta solução visa auxiliar os profissionais da área da saúde a obter os resultados mais rápidos e de forma automatizada, além de auxiliar os estudantes em sua aprendizagem.
`Detalhar <asculsensor.html#>`_


`Avalia Physio <telaavaliaphysio.html#>`_
===================
Este projeto busca desenvolver um sistema multiplataforma para auxiliar os profissionais de fisioterapia na avaliação funcional de pacientes internados na UTI através de um software que analisa a execução dos seis movimentos específicos, os quais envolvem os membros inferiores e superiores do corpo humano. Os pacientes que passarão pela avaliação funcional deverão estar cadastrados no sistema, onde devem ser registradas pelo fisioterapeuta informações pessoais para sua identificação. O profissional de fisioterapia também deverá estar cadastrado no sistema com as suas principais informações para registro do mesmo em cada avaliação realizada. O sistema possuirá um registro de cada avaliação, onde será informado o paciente e o fisioterapeuta, contendo informações sobre o movimento de cada um dos membros para que seja possível acompanhar a evolução do paciente pelo sistema. Relatórios de pacientes poderão ser gerados para acompanhamento mais preciso das avaliações realizadas ao decorrer do tempo.
`Detalhar <telaavaliaphysio.html#>`_


`Espiro Game <telaespirogame.html#>`_
===================
Este estudo visa desenvolver um sistema em hardware e software de baixo custo que auxilia em sessões de fisioterapia respiratória, fornecendo dados sobre o paciente, tornando a avaliação menos subjetiva, e conduzindo o paciente ao estado de Flow com objetivo de melhorar os resultados das sessões. Para isso, foi desenvolvido um ambiente composto um jogo para que o paciente execute diferentes atividades que estimulem a capacidade pulmonar para uma avaliação de espirometria, e um nó sensor com a placa de prototipação, juntamente com um aplicativo mobile capaz de capturar as informações de movimento por meio do acelerômetro e giroscópio. O jogo está sendo desenvolvido em Godot e é chamado de Espiro Game.

`Detalhar <telaespirogame.html#>`_


`Victus-Exergame <telavictusexergame.html#>`_
===================

Este trabalho apresenta uma solução projetada para aprimorar o processo de reabilitação física de pessoas com amputações, utilizando uma ferramenta baseada em informática médica combinada com elementos de gamificação por meio de um jogo sério. Abordando os desafios enfrentados por indivíduos com amputações de membros inferiores durante a fisioterapia - como trauma, dor e falta de motivação - este trabalho introduz um jogo sério que integra um sistema de sensores embutido com microcontroladores a uma bicicleta estacionária. Esse sistema serve tanto como controlador do jogo, quanto como um conjunto de monitores biológicos, além de ser uma ferramenta de fisioterapia que exibe os dados obtidos durante as sessões, permitindo que os terapeutas acompanhem o progresso do paciente. Desenvolvido mediante uma abordagem de design participativo envolvendo pacientes e terapeutas, o sistema coleta dados sobre o envolvimento do usuário, respostas fisiológicas e métricas de desempenho por meio de sensores.
`Detalhar <telavictusexergame.html#>`_


Projetos em Computação Aplicada com Sistemas Embarcados
**************

`Weather Station <telaweatherstation.html#>`_
===================
Construção, automatização e operação de equipamento para o monitoramento de variáveis meteorológicas e de partículas respiráveis e inaláveis suspensas na atmosfera.
A poluição atmosférica apesar de não ser considerada uma temática nova, deve ser constantemente investigada, uma vez que cerca de 7 milhões de pessoas morrem anualmente por suas consequências. Além disso, quase toda a população do mundo (99%) respira ar que excede os limites de qualidade recomendados pelas Diretrizes Globais de Qualidade do Ar, estabelecidas pela Organização Mundial de Saúde (OMS). Nesse contexto, apesar de possuir um aparato legislativo e também um considerável Sistema Nacional de Meio Ambiente (SISNAMA), o Brasil ainda apresenta deﬁciências no âmbito da poluição atmosférica, pois muitos municípios do país não realizam monitoramento, diﬁcultando os processos de controle. O estado do Rio Grande do Sul, por exemplo, possui 497 municípios e destes, apenas 5 (Canoas, Esteio, Gravataí, Guaíba e Triunfo) monitoram a poluição atmosférica. Projeto que visa contribuir com a rede de monitoramento do Rio Grande do Sul, construindo um equipamento mais acessível ﬁnanceiramente, porém com a eﬁciência dos equipamentos que já são consolidados no mercado. Bagé seria o primeiro município monitorado na Região da Campanha Gaúcha, com pretensão de expandir para os demais municípios futuramente.
`Detalhar <telaweatherstation.html#>`_


Projetos em Microeletrônica
**************

`Migortho <https://github.com/juliosaracol/migortho>`_
===================

This code base provides a framework for field-coupled technology-independent open nanocomputing in C++14 using the EPFL Logic Synthesis Libraries. fiction focuses on the physical design of emerging nanotechnologies. As a promising class of post-CMOS technologies, Field-coupled Nanocomputing (FCN) devices like Quantum-dot Cellular Automata (QCA) in manifold forms (e.g. atomic or molecular), Nanomagnet Logic (NML) devices, and many more, allow for high computing performance with tremendously low power consumption without the flow of electric current.

With ongoing research in the field, it is unclear, which technology will eventually be competing with CMOS. To be as generic as possible, fiction is able to perform physical design tasks for FCN circuit layouts on a data structure that abstracts from a particular technology or cell design. Using an extensible set of gate libraries, technologies, and cell types, these can be easily translated into whatever FCN technology is desired.

This repository contain the copy of Fiction Environment that was modified for created MIG-based synthesis published in SBCCI (Symposium on Integrated Circuits and Systems Design) 2020 named MIGortho.

`Detalhar <https://github.com/juliosaracol/migortho>`_

`IEEE SBCCI 2020 <https://ieeexplore.ieee.org/document/9189930>`_

`IEEE Design & Test <https://ieeexplore.ieee.org/document/9523547>`_


Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
