.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Victus
================
	
O projeto Victus tem como propósito disponibilizar, aos profissionais da área de fisioterapia, uma ferramenta que possibilite o acompanhamento do progresso de pacientes ao longo do tratamento de reabilitação física. A proposta é o desenvolvimento de uma solução de fácil uso, adaptável para diferentes ambientes, diferentes tipos de pacientes e, também, de baixo custo. Desta forma, o sistema Victus foi projetado para análisar sessões de fisioterapia de indivíduos amputados de membros inferiores em uma bicicleta ergométrica.

A solução foi elaborada a partir da utilização de sensores, da plataforma de prototipagem de hardware Arduino e de um software. Através dos sensores são coletados os dados de frequência cardíaca e eletromiografia (força muscular) do paciente, além da distância percorrida e velocidade alcançada durante o exercício. Assim, através da comunicação entre a plataforma Arduino e o software, os dados coletados são apresentados em tempo real para o fisioterapeuta. Ao final de cada sessão resultados parciais calculados são apresentados através de gráficos e tabelas.Além do monitoramento de um sessão em tempo real, no software é possivel cadastrar pacientes e profissionais e, também, gerar relatórios.

As sessões de reabilitação física, de pacientes amputados de membros inferiores, utilizando o sistema Victus se mostraram promissoras. Os pacientes ao utilizarem o sistema apontaram para os benefícios da ferramenta e se mostraram satisfeitos com a nova opção de tratamento. Os fisioterapeutas também demonstraram satisfação com as funcionalidades do sistema e, com a possibilidade de avaliações mais objetivas e com embasamento quantitativo.

.. image:: img/victus/victusimagem.png
.. image:: img/victus/Teste1_1.jpg
.. image:: img/victus/Teste1_2.jpg
.. image:: img/victus/Teste2_1.jpg
.. image:: img/victus/Teste2_2.jpg
.. image:: img/victus/Teste3_1.jpg
.. image:: img/victus/Teste3_2.jpg
.. image:: img/victus/Teste3_3.jpg
.. image:: img/victus/Teste3_4.jpg
.. image:: img/victus/Teste4_1.jpg

Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
