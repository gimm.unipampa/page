.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Tennisfisio
============

O presente estudo descreve a proposta de baixo custo de uma solução computacional baseada em gamificação utilizando hardware e software, que seja portável para plataformas single boards, e possa auxiliar os profissionais de fisioterapia nas sessões de reabilitação de amputações dos membros superiores e inferiores. A finalidade deste trabalho é tratar o problema descrito utilizando como sensores os do tipo acelerômetro e giroscópio de um smartphone. Foram implementados tanto o app em Flutter para coordenar a comunicação com o nó sensor, quanto o nó sensor com a ajuda da plataforma de prototipação Arduino e módulos de comunicação. Por fim, também foi desenvolvido um jogo em Unity 3D para a simulação de uma partida de tênis de mesa, e um sistema de acompanhamento para os fisioterapeutas. As informações transmitidas a partir dos sensores do smartphone, servirão para a realização da movimentação da raquete virtual no jogo. Diferentes experimentos foram executados com o objetivo de validar a proposta. Os resultados demonstraram limitações na implementação que necessitaram sua refatoração. Logo após a refatoração os resultados iniciais demonstraram grande potencialidade na solução. O projeto visa criar uma solução que torne mais lúdica e motivadora as sessões de fisioterapia, tentando levar o paciente ao estado de flow. Ademais, os fisioterapeutas terão acesso a uma ferramenta de controle de desempenho de cada uma das sessões de cada paciente, o que poderá tornar menos subjetiva a análise de desempenho dos mesmos.

.. image:: img/tennisfisio/tf1.png
.. image:: img/tennisfisio/tf2.png
.. image:: img/tennisfisio/tf3.png
.. image:: img/tennisfisio/tf4.png
.. image:: img/tennisfisio/tf5.png
.. image:: img/tennisfisio/sbgames2021.png

Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png


