.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

MFID
==========

Sabe-se que atualmente a computação está cada vez mais integrada à outras áreas de conhecimento fornecendo soluções práticas e inovadoras para todos os tipos de problemas. Na área da saúde isso também ocorre, desde equipamentos para cirurgias, ferramentas para diagnósticos de doenças, até sistemas para monitoramento e processamento de dados. Nesse sentido, o processo de reabilitação física de pacientes amputados também se mostra como uma área importante para o desenvolvimento de ferramentas que auxiliem o profissional fisioterapeuta, visto que este processo de reabilitação geralmente é duradouro e exaustivo para os pacientes. Assim, o presente trabalho descreve o desenvolvimento de uma solução para o aperfeiçoamento do processo de reabilitação física de pacientes amputados, a partir da detecção da fadiga muscular durante as sessões de exercícios físicos, que uma vez não identificada, pode causar lesões e dores aos pacientes, prolongando ainda mais o tratamento. O sistema desenvolvido consiste na integração de sensores, da plataforma de prototipagem eletrônica arduino e de um software para a visualização e armazenamento dos dados coletados. Desse modo, foi possível realizar o monitoramento e a identificação da ocorrência da fadiga muscular durante as sessões de exercícios físicos realizados pelos pacientes, além de possibilitar ao fisioterapeuta uma ferramenta para avaliação do tratamento.


.. image:: img/mfid/arquitetura.png
.. image:: img/mfid/mfid1.png
.. image:: img/mfid/mfid2.png


Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_

Apoio:
=====
.. image:: img/rodape1.png
