
.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

ARFisio
============


o objetivo deste projeto é a construção de uma solução computacional utilizando realidade aumentada e um nó sensor, o qual possibilite a simulação de um conjunto de atividades necessárias ao processo de reabilitação física de pacientes com amputações em membros superiores. Desta forma, através da interatividade e imersão do paciente, estimulem áreas motoras do córtex e de feedback visual. Vislumbra-se também, que este estudo disponibilize um instrumento válido para fisioterapeutas acompanharem a evolução de seus pacientes para que os mesmos tenham uma melhor adaptação a sociedade.

Como atividade inicial modelou-se um braço virtual, utilizado para representar uma prótese, o qual foi concebido com base em uma armadura de 21 ossos, a fim de permitir que esta prótese execute movimentos tangíveis. Os movimentos básicos que podem ser executados 
pela prótese virtual são: fechamento da mão, flexão dos dedos: polegar, indicador, médio, anelar e mindinho. No intuito de aprimorar a experiência com a prótese, foram projetados  movimentos mais refinados/complexos, como pegar uma xícara sobre a mesa, movimentá-la e deixa-la em qualquer ponto da mesma superfície.

Até o momento foi efetuada a implementação inicial do projeto, por meio da qual identificou-se que o protótipo apresentou um desempenho desejável, na execução  de movimentos básicos de flexão dos dedos, bem como na interação desta prótese com  outro objeto virtual.

.. image:: img/arfisio/arfisio1.png
.. image:: img/arfisio/arfisio2.png


Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
