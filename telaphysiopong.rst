.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Physiopong
===========

A aplicação intitulada PhysioPong, propõe uma solução computacional baseada no contexto de Serious Games (Jogos Sérios), para apoiar no processo de reabilitação física de pacientes amputados de membro superior. Dessa forma, espera-se fornecer uma ferramenta de  treinamento, utilizando a plataforma de prototipação Arduino integrada a técnicas de monitoramento de sinais eletromiográficos para auxiliar nas primeiras fases do processo de
reabilitação destes pacientes. Assim, a solução computacional deverá possibilitar a simulação de um conjunto de atividades necessárias ao processo de reabilitação física destes indivíduos. Vislumbra-se também, proporcionar ao paciente um ambiente motivador para o tratamento, bem como uma ferramenta válida para os fisioterapeutas acompanharem a evolução destes indivíduos.

A aplicação é baseada em uma versão simplificada do jogo conhecido por Pong, desenvolvido pela Atari, de modo que simula tênis de mesa em duas dimensões, com o objetivo de rebater a bolinha, evitando que esta toque a parte inferior da tela. Os movimentos da paletas são controlados através de sensores EMG posicionados no coto do paciente.

Com o intuito de avaliar a aplicação, foi realizado um teste com um paciente amputado Transradial (amputado abaixo do cotovelo) selecionado através dos fisioterapeutas do SRF, que acompanharam todo o processo. Dessa forma, foi possível identificar que o protótipo implementado possibilitou ao paciente controlar as paletas de maneira satisfatória, além de constatar, com o feedback do indivíduo, o alto nível de motivação 
alcançado com o jogo.

.. image:: img/physiopong/pong1.png
.. image:: img/physiopong/pong2.jpg
.. image:: img/physiopong/pong3.jpg
.. image:: img/physiopong/pong4.jpg
.. image:: img/physiopong/pong6.jpg

Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
