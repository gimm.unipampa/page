
.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

FootInspect - Baropodometro
===========================

O projeto FootInspect, tem por finalidade, a criação de um sistema computacional que dê suporte à fisioterapeutas e profissionais da área da saúde, ajudando na análise, diagnóstico, de pessoas que possuem problemas relacionados às regiões plantares (pés), visando a reabilitação física e a melhorar a qualidade de vida dos indivíduos. Basicamente, a solução dispõe de uma palmilha sensível a pressão, de baixo custo, para assessorar sessões de fisioterapia através do monitoramento  da distribuição de pressão na planta do pé.

O objetivo deste projeto é desenvolver um protótipo de sistema de palmilhas sensíveis a pressão, também chamado de baropodômetro, com elementos sensores e a plataforma de prototipagem Arduino, além de um software auxiliar para análise dos dados coletados durante uma sessão de avaliação. Através do Arduino e dos sensores FSR (Force Sensor Resistor) são coletados dados referentes à distribuição de pressão plantar exercida durante a marcha, os dados coletados são processados pelo software que gera  indicadores aos quais os fisoterapeutas podem utilizar para analizar os padrões de distribuição de pressão plantar.

Os resultados definidos até o momento, são a validação da arquitetura de hardware, através de testes  dos dispositivos, foram testados a integração dos sensores FSR com o módulo de armazenamento, e coletado dados da pressão plantar, os dados foram interpolados com técnicas de geoestatística e apresentados aos profissionais fissioterapeutas. Considerando os resultados obtidos é possível concluir que o projeto possui potencial para satisfazer os objetivos de auxílio aos profissionais e no diagnóstico e tratamento de doenças plantares. Além disso, outra aplicação potencial seria a utilização do protótipo para o auxílio na criação de palmilhas ortopédicas.

.. image:: img/footinspect/foot1.png
.. image:: img/footinspect/foot2.jpg
.. image:: img/footinspect/foot3.png
.. image:: img/footinspect/foot4.png
.. image:: img/footinspect/foot5.png
.. image:: img/footinspect/foot6.png


Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
