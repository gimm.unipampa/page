.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Espiro Game
================
	
A área da Fisioterapia possui diversos métodos de tratamentos para problemas de saúde, principalmente no contexto de reabilitação física. Nesse sentido, a fisioterapia respiratória consiste na aplicação de técnicas para a melhora da capacidade respiratória em diferentes aspectos, por exemplo, a técnica de vibração torácica manual que é extremamente importante para a higienização brônquica. Essa técnica tem como objetivo remover secreções contidas nas vias respiratórias e pulmões. Essas técnicas ampliam a capacidade respiratória e fortalecem os músculos como o do diafragma, que é responsável pelo funcionamento dos pulmões. Para isso, o profissional de saúde utiliza certos aparelhos e técnicas que permitem fortalecer a musculatura pulmonar do paciente gradualmente. Entretanto as sessões de reabilitação física pulmonar podem ser desmotivadoras e ou até mesmo entediantes, o que se torna ainda mais desafiador para o público infantil. Desta forma é preciso buscar soluções lúdicas e que auxiliem os profissionais a tornarem o ambiente das sessões mais motivantes. Este trabalho propõe uma solução computacional baseada em jogos sérios para estimular e auxiliar pacientes que possuem algum tipo de dificuldade ou problema na parte respiratória e que necessitam de sessões de fisioterapia respiratória. Foi proposto um jogo sério que associado a um conjunto de nó sensores poderão interagir com o ambiente do jogo. O jogo está sendo desenvolvido em Godot Engine para executar em máquinas com baixa performance ou single boards como a Raspberry Pi. Esse trabalho propõe a criação de um ambiente focado no treinamento pulmonar, onde possuirá alguns sensores para ajudar na averiguação do desempenho e do desenvolvimento do paciente durante o uso do software (jogo) nas sessões de fisioterapia respiratória, sendo capaz de criar gráficos que irão apresentar alguns indicadores relevantes, como a força do músculo do diafragma e a capacidade pulmonar do paciente. Como resultados, espera-se obter um sistema de treinamento, auxílio e acompanhamento de pacientes em fisioterapia respiratória, onde seja capaz de produzir informações estatísticas sobre a evolução do paciente, tornando o processo de avaliação menos subjetivo. Além disso, espera-se propor novos protocolos de treinamento respiratório por meio de jogos sérios, principalmente com o enfoque no público infantil. 


.. image:: img/espirogame/espirogamemenu1.jpeg
.. image:: img/espirogame/espirogamemenu2.jpeg
.. image:: img/espirogame/espirogamesoprar.jpeg
.. image:: img/espirogame/espirogamezarabatana.jpeg


Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png

