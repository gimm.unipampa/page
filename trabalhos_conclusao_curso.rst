.. image:: img/bannergimm.png


Trabalhos de Conclusão de Curso
********************************

MAURICIO DE SOUZA REALAN ARRIEIRA. VICTUS: PROPOSTA DE UMA SOLUÇÃO COMPUTACIONAL APLICADA NA REABILITAÇÃO FÍSICA DE INDIVÍDUOS AMPUTADOS DE MEMBROS INFERIORES. 2017. Trabalho de Conclusão de Curso. (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Érico Marcelo Hoff do Amaral.

DANIEL MEIRELLES AFFELDT. PROJETO DE DESENVOLVIMENTO DE UM PROTÓTIPO DE BAROPÔDOMETRO. 2018. Trabalho de Conclusão de Curso. (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Julio Saraçol Domingues Júnior.

JONE FOLLMANN. SERIOUS GAME APLICADO AO PROCESSO DE REABILITAÇÃO FÍSICA DE AMPUTADOS DE MEMBRO SUPERIOR. 2019. Trabalho de Conclusão de Curso. (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Érico Marcelo Hoff do Amaral.

BRYAN TEIXEIRA PAIVA. DESENVOLVIMENTO DE UM PROTÓTIPO DE IDENTIFICAÇÃO DA FADIGA MUSCULAR EM SESSÕES DE EXERCÍCIOS FÍSICOS BASEADO NO AMBIENTE DA FERRAMENTA VICTUS. 2019. Trabalho de Conclusão de Curso. (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Érico Marcelo Hoff do Amaral.

GABRIEL LADEIA DE SOUZA COSTA. VICTUS VR: UMA APLICAÇÃO DE REALIDADE VIRTUAL PARA ESTÍMULO E SUPORTE À RECUPERAÇÃO DE INDIVÍDUOS AMPUTADOS DE MEMBROS INFERIORES. 2018. Trabalho de Conclusão de Curso. (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Érico Marcelo Hoff do Amaral.

ANDRELISE NUNES LEMOS PINHEIRO. Proposta de Solução no Apoio de Sessões de Fisioterapia para Amputados Baseada em Gamificação. 2021. Trabalho de Conclusão de Curso (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Julio Saraçol.


ARTHUR TEIXEIRA. FISIOTERAPIA RESPIRATÓRIA: PROPOSTA DE UM SISTEMA PARA O MONITORAMENTO DE SESSÕES - UNIPAMPA. 2021. Trabalho de Conclusão de Curso (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Érico Marcelo Hoff do Amaral.

RODRIGO ACOSTA SEGUI. BOTPAMPA: PROPOSTA DE DESENVOLVIMENTO DE UM CHATBOT PARA A UNIVERSIDADE FEDERAL DO PAMPA - UNIPAMPA. 2021. Trabalho de Conclusão de Curso (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Julio Saraçol.

GIULIANA OLIVEIRA DE MATTOS LEON. Ausculsensor: Ausculta Automática Para Fisioterapia Respiratória. 2022. Trabalhos de Conclusão de Curso (Graduação em Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Julio Saraçol. 

MATHEUS DE JESUS MARQUES. Aplicativo Mobile E Web Para Auxílio De Fisioterapeutas Na Avaliação Funcional Dentro De Unidades De Tratamento Intensivo. 2023. Curso (Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Julio Saraçol.

DOUGLAS AQUINO TEIXEIRA MENDES. Game Physio - Ambiente de Reabilitação Física para Amputados Baseado em Gamificação. 2023. Curso (Engenharia de Computação) - Universidade Federal do Pampa. Orientador: Julio Saraçol.

GIULIANA OLIVEIRA DE MATTOS LEON. Proposta De Uma Heurística Para Reconhecimento De Pragas Baseada Em Deep Learning Para Armadilhas Inteligentes. 2023. Dissertação (COMPUTAÇÃO) - Universidade Federal de Pelotas. Co-orientador: Julio Saraçol

Rafael Luz Melo. Victus Exergame: Uma Abordagem De Baixo Custo Baseada Em Jogos Sérios Para Fisioterapia Em Pessoas Com Amputação De Membro Inferior. 2024. Curso (Engenharia de Computação) - Universidade Federal do Pampa. Em andamento.  Orientador: Julio Saraçol. 
 

Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png

