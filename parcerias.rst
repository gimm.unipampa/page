.. image:: img/bannergimm.png

Parcerias
**************


Empresas Parceiras
======================
* `UNIMED- Região da Campanha <https://www.unimed.coop.br/web/regiaodacampanha>`_, 
* `Ortopédica Canadense <https://www.facebook.com/OrtopedicaCanadenseLtda/>`_ 


Instituições Parceiras
======================
*  `Oficina Ortopédica de Bagé <https://www.google.com/search?q=oficina+ortopedica+de+bag%C3%A9&oq=oficina+ortopedica+de+bag%C3%A9&aqs=chrome..69i57j0i131i433i512j46i433i512j0i433i512j0i131i433j0i131i433i512j0i131i433j46i175i199i433j0i131i433l2.4195j0j7&sourceid=chrome&ie=UTF-8#lpg=cid:CgIgAQ%3D%3D,ik:CAoSLEFGMVFpcFBkQzRoQnpsdEN5VTEtVy1wcUpnNzdpak5YYlQtejNUbzZOMW9I>`_
*  `Serviço de Reabilitação Física de Bagé (SRF) <https://www.youtube.com/watch?v=qkFYKmjYp0c>`_

Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Publicações <publicacoes.html#>`_
* `Parcerias <parcerias.html#>`_

Apoio:
=====
.. image:: img/rodape1.png
