

.. image:: img/bannergimm.png


Sejam Bem Vindos a Página do GIMM:


Grupo de Informática Médica e Microeletrônica
**********************************************************

.. image:: img/logo_gimm.png


O Grupo de Informática Médica e Microeletrônica (GIMM) do curso de Engenharia de Computação da Universidade Federal do Pampa, tem como objetivo o estudo e desenvolvimento de soluções computacionais aplicaveis na área da saúde e microeletrônica, atuando em soluções que envolvam sistemas embarcados, aplicações em diferentes linguagens de programação e plataformas. 

O grupo foi fundado em 2016 com os professores Érico Amaral e Julio Saraçol, líderes do grupo. Diversos trabalhos têm sido desenvolvidos nos últimos anos através de projetos de pesquisa e trabalhos de conclusão de curso, os quais originaram diferentes publicações em eventos científicos da comunidade. Além disso, o grupo possui importantes parcerias com empresas e instituições de saúde tanto da cidade de Bagé, quanto da região, dentre elas cita-se: `UNIMED- Região da Campanha <https://www.unimed.coop.br/web/regiaodacampanha>`_, `Ortopédica Canadense <https://www.facebook.com/OrtopedicaCanadenseLtda/>`_ , `Oficina Ortopédica de Bagé <https://www.google.com/search?q=oficina+ortopedica+de+bag%C3%A9&oq=oficina+ortopedica+de+bag%C3%A9&aqs=chrome..69i57j0i131i433i512j46i433i512j0i433i512j0i131i433j0i131i433i512j0i131i433j46i175i199i433j0i131i433l2.4195j0j7&sourceid=chrome&ie=UTF-8#lpg=cid:CgIgAQ%3D%3D,ik:CAoSLEFGMVFpcFBkQzRoQnpsdEN5VTEtVy1wcUpnNzdpak5YYlQtejNUbzZOMW9I>`_ e `Serviço de Reabilitação Física de Bagé (SRF) <https://www.youtube.com/watch?v=qkFYKmjYp0c>`_. É importante salientar que essa parceria com o SRF viabiliza o suporte técnico de experientes fisioterapeutas nos trabalhos que envolvem a área médica. A página do GIMM no diretório de grupos de pesquisa do Brasil do Conselho Nacional de Desenvolvimento Cíentifico e Técnológico - CNPq é encontrada `Aqui <http://dgp.cnpq.br/dgp/espelhogrupo/770634>`_.


.. toctree::
	::maxdepth: 5
	equipe
	projetos
	publicacoes
	parcerias
	projetos_de_pesquisa
	trabalhos_conclusao_curso
	identidade_visual


Apoio:
*********
.. image:: img/rodape1.png



   
