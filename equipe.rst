.. image:: img/bannergimm.png

Equipe
**************

Líderes
===============
* Érico Amaral - Docente Unipampa - Doutor em Informática na Educação - `lattes <http://lattes.cnpq.br/2530535838251633>`_ 

* Julio Saraçol - Docente Unipampa - Doutor em Ciência da Computação - `lattes <http://lattes.cnpq.br/4942149493804162>`_

Alunos
================
* Fernando Colosio - Engenharia de Computação - `lattes <http://lattes.cnpq.br/6749999605734776>`_
* Willian Silva Domingues - Engenharia de Computação - `lattes <http://lattes.cnpq.br/8022315493250800>`_
* André Francisco Silva de Farias - Engenharia de Computação - `lattes <http://lattes.cnpq.br/0190906442002157>`_
* Rafael Luz Melo - Engenharia de Computação - `lattes <http://lattes.cnpq.br/6421659220679452>`_
* Tiago Machado Jardim - Engenharia de Computação - `lattes <http://lattes.cnpq.br/9279305328822367>`_
* Vitor da Silva Ferreira - Engenharia de Computação - `lattes <http://lattes.cnpq.br/4835831312325354>`_
* Vitor da Silva Moreira - Engenharia de Computação - `lattes <http://lattes.cnpq.br/9339730799459988>`_
* Luís Felipe Caleal Maccalli - Engenharia de Computação - `lattes <http://lattes.cnpq.br/6588488953151591>`_
.. * João Antônio dos Santos Antunes - Engenharia de Computação - `lattes <http://lattes.cnpq.br/>`_
.. * Patrick Tarouco - Engenharia de Computação - `lattes <http://lattes.cnpq.br/5015315575358669>`_
.. * Gabriel Brinhol - Engenharia de Computação - `lattes <http://lattes.cnpq.br/7561476132601104>`_
.. * Tomás Rossato Benfica - Engenharia de Computação - `lattes <>`_
.. * Indra Rani Araújo Verissimo dos Santos - Engenharia de Computação - `lattes <http://lattes.cnpq.br/3766943528390898>`_


Egressos
===============
* Maurício de Souza Realan Arrieira - `lattes <http://lattes.cnpq.br/8821377395149548>`_
* Daniel Meirelles Affeldt - `lattes <http://lattes.cnpq.br/1517161364706655>`_
* Gabriel Ladeia de Souza Costa - `lattes <http://lattes.cnpq.br/3242185568373522>`_
* Jone Follmann - `lattes <http://lattes.cnpq.br/1409018444044997>`_
* Bryan Teixeira Paiva - `lattes <http://lattes.cnpq.br/6082926973475798>`_
* Arthur Teixeira - Engenharia de Computação - `lattes <http://lattes.cnpq.br/3699740397379011>`_
* Andrelise Nunes Lemos Pinheiro - Engenharia de Computação - `lattes <http://lattes.cnpq.br/4449891543009123>`_ 
* Rodrigo Acosta Segui - Engenharia de Computação - `lattes <http://lattes.cnpq.br/3201060052300860>`_
* Giuliana Oliveira De Mattos Leon - Engenharia de Computação - `lattes <http://lattes.cnpq.br/2898029118234269>`_
* Matheus de Jesus Marques - Engenharia de Computação - `lattes <http://lattes.cnpq.br/5463852124245818>`_
* Douglas Aquino Teixeira Mendes - Engenharia de Computação - `lattes <http://lattes.cnpq.br/7569889022353205>`_


Colaborares
=================
* Alice Finger - Docente Unipampa - `lattes <http://lattes.cnpq.br/2691501072064698>`_
* Danuza Correa - Fisioterapeuta - `lattes <http://lattes.cnpq.br/3136281662903297>`_
* Leonardo Pinho - Docente Unipampa - `lattes <http://lattes.cnpq.br/8935551668939735>`_
* Márcio Barroso - Fisioterapeuta - `lattes <http://lattes.cnpq.br/5583052948471988>`_


.. image:: img/equipe.jpg

.. image:: img/ortopedica1.jpg

.. image:: img/ortopedica2.jpg

.. image:: img/pessoal.jpg

.. image:: img/pessoal2.jpeg

Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
